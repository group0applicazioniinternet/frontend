import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import * as jwt_decode from "jwt-decode";
import { ApiService } from "./api.service";
import { ViewControlService } from "./view-control.service";
import { Router } from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient,
    private cookieService: CookieService,
    private API: ApiService,
    private VCS: ViewControlService,
    private router: Router) { }


  private token: string = null;

  //Used for oAuth 2
  private clientId = "Client1";
  private clientSecret = "Secret1";


  login(username, password) {
    const apiUrl = "http://localhost:8080/oauth/token";
    let body = new URLSearchParams();

    body.set("grant_type", "password");
    body.set("username", username);
    body.set("password", password);

    let headers = new HttpHeaders();
    headers = headers.set("Authorization", "Basic " + btoa(this.clientId + ":" + this.clientSecret));
    headers = headers.set("Content-Type", "application/x-www-form-urlencoded");

    return this.http.post(apiUrl, body.toString(), { headers: headers });
  }

  saveToken(token: string) {
    let expiredDate = new Date();
    expiredDate.setDate(expiredDate.getDate() + 7);
    this.token = token;
    this.API.useToken(this.token);

    this.cookieService.set('token', this.token, expiredDate);

    //console.log("Saving token: " + token);
  }

  logout() {
    this.VCS.change("logout"); //Update toolbar buttons

    this.cookieService.set('token', '', 0);
    this.cookieService.delete('token');
    this.cookieService.deleteAll();

    this.token = null;
    this.API.useToken(null); //Invalidating token for API calls

    this.router.navigate(['/login']); //Redirect
  }

  getToken() {
    return this.token;
  }

  isLogged() {
    let cookieToken;

    //Check if there is a token with the cookie
    if (this.token == null) {
      cookieToken = this.cookieService.get('token');
      if (cookieToken == null) {
        return false;
      }
    } else {
      cookieToken = this.token;
    }

    //Verify it is not expired
    try {
      let tokenInfo = jwt_decode(cookieToken);
      let expireDate = tokenInfo.exp;
      let timeStamp = Math.floor(Date.now() / 1000);
      let difference = expireDate - timeStamp;

      if (difference <= 0) {
        return false;
      } else {
        this.token = cookieToken;
        this.API.useToken(this.token);
        this.VCS.change("login"); //Change toolbar buttons
        return true;
      }
    } catch (Error) {
      return false;
    }
  }




  signUp(username, password) {

    const apiUrl = "http://localhost:8080/registration";
    let body = new URLSearchParams();

    let credentials = '{ "username": "' + username + '", "password": "' + password + '" }';
    body.set("credentials", credentials);
    let headers = new HttpHeaders();
    headers = headers.set("Authorization", "Basic " + btoa(this.clientId + ":" + this.clientSecret));
    headers = headers.set("Content-Type", "application/x-www-form-urlencoded");

    return this.http.post(apiUrl, body.toString(), { headers: headers });
  }


}
