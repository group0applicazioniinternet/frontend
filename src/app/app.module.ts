import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';


import { MyMaterialModule } from './my-material/my-material.module';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { AppRoutingModule } from './/app-routing.module';
import { FormsModule } from '@angular/forms';
import { UserMapComponent } from './pages/user/user-map/user-map.component';
import { UserViewInsertComponent } from './pages/user/user-view-insert/user-view-insert.component';
import { CookieService } from 'ngx-cookie-service';

import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { UserBuyOverviewComponent } from './pages/user/user-map/user-buy-overview/user-buy-overview.component'; //Map
import { Ng2GoogleChartsModule } from 'ng2-google-charts';

import { HttpClientModule } from '@angular/common/http';

import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    HomeComponent,
    LoginComponent,
    UserMapComponent,
    UserViewInsertComponent,
    UserBuyOverviewComponent
  ],
  imports: [
    BrowserModule,
    MyMaterialModule,
    AppRoutingModule,
    LeafletModule.forRoot(), //Map
    FormsModule,
    Ng2GoogleChartsModule, //Google Chart
    HttpClientModule, //For HTTP requests

    OwlDateTimeModule, OwlNativeDateTimeModule //Datetimepicker
  ],
  entryComponents: [
    UserBuyOverviewComponent //To show buy overview dialog when buying new positions
  ],
  providers: [ CookieService ], //To save cookie to restore login token
  bootstrap: [AppComponent]
})
export class AppModule { }