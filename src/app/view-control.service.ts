import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ViewControlService {

  constructor(public snackBar: MatSnackBar) { }

  EE: EventEmitter<any> = new EventEmitter();

  //This function is called when it is needed to change the toolbar buttons
  change(message:string) {
    this.EE.emit(message);
 }

  //This function is called to get the event emitter and to subscribe to it.
  //It is called from the toolbar to bind itself to the emitter and receive messages
  //from other part of the application (login/signup and logout)
  getEmittedValue() {
    return this.EE;
  }


  //This function opens a little popup at the bottom of the window.
  openSnackbar(message:string, duration:number = 3000){
    this.snackBar.open(message, 'Ok', {duration: duration});
  }

}
