import { Component, OnInit } from '@angular/core';
import { ViewControlService } from '../../view-control.service';
import { AuthService } from '../../auth.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {

  constructor(private Vcs: ViewControlService, private auth: AuthService) { }

  showLogin: boolean = true;
  showUser: boolean = false;
  showMap: boolean = false;
  showLogout: boolean = false;


  ngOnInit() {
    this.Vcs.getEmittedValue()
      .subscribe(item => {

        if (item == "login") {
          this.showLogin = false;
          this.showUser = true;
          this.showMap = true;
          this.showLogout = true;
        }

        if (item == "logout") {
          this.showLogin = true;
          this.showUser = false;
          this.showMap = false;
          this.showLogout = false;
        }

      });
  }

  logout() {
    this.auth.logout();
  }

}
