import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { UserMapComponent } from './pages/user/user-map/user-map.component';
import { UserViewInsertComponent } from './pages/user/user-view-insert/user-view-insert.component';
import { AuthGuard } from './auth.guard'

const routes: Routes = [
  {path: '', component: HomeComponent, pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'user/map', component: UserMapComponent, canActivate: [AuthGuard]},
  {path: 'user/view-insert', component: UserViewInsertComponent, canActivate: [AuthGuard]}
]


@NgModule({
  exports: [ RouterModule ],
  imports: [RouterModule.forRoot(routes)]
})
export class AppRoutingModule {}

