import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';

import { NgForm } from '@angular/forms';
import { Router } from "@angular/router";
import { AuthService } from '../../auth.service';
import { ViewControlService } from '../../view-control.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private auth: AuthService,
    private router: Router,
    private Api: ApiService,
    private Vcs: ViewControlService) { }

  ngOnInit() { }

  //Default values to speed up testing
  //Fields ending with L are binded to the Login Tab
  //Fields ending with S are binded to the Signup Tab
  usernameL: String = "Sara";
  passwordL: String = "pass2";
  usernameS: String;
  passwordS: String;
  passwordSRepeat: String;
  loadingProgress: boolean; //Variable tested in the template to show the progress bar


  //This function is a wrapper for the performLogin
  loginUser(loginForm: NgForm) {
    if (loginForm.valid) {
      this.performLogin(this.usernameL, this.passwordL);
    } else {
      //console.log("Form not valid!");
      this.Vcs.openSnackbar("Form not valid!");
    }
  }//End of loginUser(...)


  signUpUser(signUpForm: NgForm) {
    
    if (signUpForm.valid && this.passwordS == this.passwordSRepeat) {
      this.loadingProgress = true;

      this.auth.signUp(this.usernameS, this.passwordS).subscribe(
        (data) => {
          this.loadingProgress = false;

          //console.log("Data received from signUp: ", data); //DEBUG
          if (!(<any>data).success) {
            this.Vcs.openSnackbar((<any>data).message);
          } else {
            this.performLogin(this.usernameS, this.passwordS);
          }
        },
        (error) => {
          //console.log("Request failed -> ", error);
          this.loadingProgress = false;
          //console.log(error);
          this.Vcs.openSnackbar("Something went wrong!");
        });

    } else {
      this.loadingProgress = false;
      this.Vcs.openSnackbar("Form not valid!");
    }
  }//End of signUpUser(...)


  //Thi is the function that actually performs the login after the login tab or
  //after the user was signed up
  performLogin(usnm, pswd) {
    this.loadingProgress = true; //Loading bar set

    this.auth.login(usnm, pswd).subscribe(
      (data) => {
        //console.log(data); //DEBUG
        this.loadingProgress = false; //Loading complete
        this.auth.saveToken(data["access_token"]); //Save token (cookie)
        this.Vcs.change("login"); //Change toolbar buttons
        this.router.navigate(['/user/view-insert']); //Redirect to user main page
      },
      (error) => {
        this.loadingProgress = false;
        this.Vcs.openSnackbar("Something went wrong!");
      });
  }//End of performLogin(...)

}