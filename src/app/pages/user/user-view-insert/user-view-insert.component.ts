import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../api.service';
import { ViewControlService } from '../../../view-control.service';

@Component({
  selector: 'app-user-view-insert',
  templateUrl: './user-view-insert.component.html',
  styleUrls: ['./user-view-insert.component.css']
})
export class UserViewInsertComponent implements OnInit {

  constructor(private Api: ApiService, private Vcs: ViewControlService) { }

  loadingProgress: boolean = true;

  userArchives: Object;
  userBoughtArchives: Object;

  displayedColumns = ['timestamp', 'latitude', 'longitude']; //For the tables (both of them)
  dataSource: Object[]; //Vector of positions

  fileToUpload: File = null;
  textFile: String;
  fileName: String = null;

  ngOnInit() {
    //Loading
    this.getUserArchiveFunction();
    this.getUserBoughtArchivesFunction();
  }


  getUserArchiveFunction() {
    this.Api.getUserArchives().subscribe((data) => {
      this.loadingProgress = false;
      this.userArchives = (<any>data).fields.archives;
      //console.log("User archives: ", this.userArchives);
      this.Vcs.openSnackbar("Loading complete!");
    }, (error) => {
      console.log(error);
      this.loadingProgress = false;
      this.Vcs.openSnackbar("Something went wrong!");
    });
  }


  getUserBoughtArchivesFunction() {
    this.Api.getUserBoughtArchives().subscribe((data) => {
      this.loadingProgress = false;
      //console.log("Bought archives: ", data); 
      this.userBoughtArchives = (<any>data).fields.archives_bought;

      this.Vcs.openSnackbar("Loading complete!");
    }, (error) => {
      console.log(error);
      this.loadingProgress = false;
      this.Vcs.openSnackbar("Something went wrong!");
    });
  }


  //This function is binded to the view
  handleInputFile(files: FileList) {
    this.fileToUpload = files.item(0);
    //console.log(this.fileToUpload); //DEBUG
    this.fileName = this.fileToUpload.name;

    let fileReader = new FileReader();
    fileReader.readAsText(this.fileToUpload);

    fileReader.onload = (e) => {
      this.textFile = fileReader.result;
    }
  }

  sendInputFile() {
    this.Api.addUserArchives(this.textFile).subscribe((data) => {
      this.loadingProgress = false;
      //console.log(data);
      this.Vcs.openSnackbar("Loading complete!");

      this.getUserArchiveFunction();
    }, (error) => {
      console.log(error);
      this.loadingProgress = false;
      this.Vcs.openSnackbar("Something went wrong!");
    });
  }

  deleteArchive(archiveId, index) {
    this.loadingProgress = true;
    this.Api.deleteUserArchive(archiveId).subscribe((data) => {
      this.loadingProgress = false;
      //console.log(data); //DEBUG
      this.userArchives[index].deleted = true; //Deleteing local copy (to show the red box immediately)
      this.Vcs.openSnackbar("Delete complete!");
    }, (error) => {
      console.log(error);
      this.loadingProgress = false;
      this.Vcs.openSnackbar("Something went wrong!");
    });
  }


  //This function generates a file and let the user download the archive
  download(filename, archive) {
    let element = document.createElement('a');
    let text = JSON.stringify(archive);
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  }


}
