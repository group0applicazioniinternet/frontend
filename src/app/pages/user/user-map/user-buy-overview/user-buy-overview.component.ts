import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-user-buy-overview',
  templateUrl: './user-buy-overview.component.html',
  styleUrls: ['./user-buy-overview.component.css']
})
export class UserBuyOverviewComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<UserBuyOverviewComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Object) { }

  ngOnInit() { }


  //This component is loaded from a dialog.

  displayedColumns: string[] = ['archiveId', 'bought']; //For the table

  //Receiving arguments to bind to the view
  dataSource = this.data[0];
  positionCounter = this.data[1];

  buy() {
    this.dialogRef.close("buy"); //Returns the string buy to the component that opened the dialog
  }

  cancel() {
    this.dialogRef.close("cancel"); //Returns the string cancel to the component that opened the dialog
  }

  confirm() { //When there are no position
    this.dialogRef.close("confirm"); //Returns the string confirm to the component that opened the dialog
  }

}
