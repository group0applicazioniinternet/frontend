import { Component, OnInit, ChangeDetectorRef, Inject } from '@angular/core';
import { latLng, Map, polygon, circle, tileLayer, LatLng } from 'leaflet';

import { MatDialog } from '@angular/material';
import { ApiService } from '../../../api.service';
import { UserBuyOverviewComponent } from './user-buy-overview/user-buy-overview.component';
import { ViewControlService } from '../../../view-control.service';

@Component({
  selector: 'app-user-map',
  templateUrl: './user-map.component.html',
  styleUrls: ['./user-map.component.css']
})
export class UserMapComponent implements OnInit {

  constructor(private cd: ChangeDetectorRef,  //To update map view in some cases
    private Api: ApiService, //Holds API calls to our backend
    public dialog: MatDialog, //For buy popup
    private Vcs: ViewControlService //Service that controls the toolbar and some small informative popups (snackbar)
  ) { }

  ngOnInit(): void { }

  mapOptions = {
    layers: [
      tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
        { maxZoom: 18, attribution: "Group 0 App" })
    ],
    zoom: 14,
    center: latLng(45.0624443, 7.6623604) //Turin
  };

  layers = []; //Circles and a polygon (drawed on map) will end up here
  drawnPolygon = polygon([]); //When the user clicks on the map it will add points to this structure

  //Time strings binded to the view
  startTime = new Date(2017, 1, 1, 10, 30, 0);//"2017.01.01.10.30.00";
  endTime = new Date(2020, 4, 27, 9, 35, 0);; //"2020.04.27.9.35.00";

  //When the map is loaded or the user pans on the map this structure is updated to be sent to the API
  //in order to load the new archives with approximate positions and timestamps.
  //The polygon in this structure corresponds to the current map view
  boundObject: Object = { timestamp: { start: "", end: "" }, polygon: [] };

  //This structure is identical to the boundObject but instead it is used to hold the points of the
  //polygon drawn on the map by the user
  requestObject: Object = { timestamp: { start: "", end: "" }, polygon: [] }; //

  bubbleChartData: Object; //Structure for the bubble chart
  userColors = new Object(); //It is like a map. It holds the generated random color for users seen at least once
  approxTimeStamps = {}; //t is used to show data on the bubblechart
  usersForListUsers = []; //List of ids to show with checkboxes, to exclude them.

  GMap: Map; //GlobalMapObject


  onMapReady(map: Map) {
    this.GMap = map;
    //First time the map is loaded call the API to load archives
    this.updateBoundObject(map);
    //this.updateArchivesStructures();

    //User moving on map event
    map.on("moveend", (e: any) => {
      this.updateBoundObject(map); //For the "requestNewArchives" request;
      //this.updateArchivesStructures();
    });

    //User drawing polygon on map
    map.on("click", (e: any) => {
      let newPos = { lat: e.latlng.lat, long: e.latlng.lng }; //Getting position from map
      let newPosForSpring = { lat: e.latlng.lng, long: e.latlng.lat }; //Casting

      this.addPointInPolygon(newPos, newPosForSpring); //Update map
      this.cd.detectChanges();
    });
  }

  generateBubbleChartData(usersWTimestamps) {
    //console.log("Timestamps ricevuti: ", usersWTimestamps);

    this.bubbleChartData = {
      chartType: 'BubbleChart',
      dataTable: [
        ['PositionID', 'TimeOfDay', 'yAxis', 'User', 'Weight'],
        ['', [-5, 0, 0], 0, '1', 1], //Serve per poter renderizzare il grafico anche da vuoto
      ],
      options: {
        hAxis: { title: '', viewWindow: { min: [0, 0, 0], max: [23, 59, 59, 59] }, gridlines: { count: 6 } },
        vAxis: { title: '', gridlines: { count: 0 } },
        legend: { position: 'none' },
      },
    };


    for (var user in usersWTimestamps) {
      usersWTimestamps[user].forEach(currentTimestamp => {
        let date = new Date(currentTimestamp);
        let hh = date.getHours();
        let mm = date.getMinutes();
        (<any>this.bubbleChartData).dataTable.push(['', [hh, mm, 0], 0, user, 1]);
      });
    }

  }

  //This function is called when the user pans the map
  updateBoundObject(map: Map) {
    let i, now;
    let boundaries: Object[] = [];

    now = this.getFormattedNow();

    //Input from user
    (<any>this.boundObject).timestamp.start = this.convertDate(this.startTime);
    (<any>this.boundObject).timestamp.end = this.convertDate(this.endTime);

    boundaries[0] = map.getBounds().getNorthWest();
    boundaries[1] = map.getBounds().getNorthEast();
    boundaries[2] = map.getBounds().getSouthEast();
    boundaries[3] = map.getBounds().getSouthWest();

    for (i = 0; i < 4; i++) {
      (<any>this.boundObject).polygon[i] = new Object();
      (<any>this.boundObject).polygon[i].lat = (<any>boundaries[i]).lng;
      (<any>this.boundObject).polygon[i].long = (<any>boundaries[i]).lat;
    }

    this.updateArchivesStructures();
  }


  //Binded to the buy button on the view.
  //This function checks if the polygon has at least three edges.
  buy() {
    if ((<any>this.requestObject).polygon.length < 3) {
      this.Vcs.openSnackbar("Devi disegnare un poligono >= 3 lati!");
      return;
    }

    //Retriving timestamps from the view
    (<any>this.requestObject).timestamp.start = this.convertDate(this.startTime);
    (<any>this.requestObject).timestamp.end = this.convertDate(this.endTime);

    if(this.endTime < this.startTime){
      this.Vcs.openSnackbar("Errore nella selezione delle date!");
      return;
    }

    //Call the same function called from the map load but with a different request.
    //This time the object (requestObject) holds the edges of the polygon drawn on the map by the user
    this.Api.getPositions(this.requestObject).subscribe((data) => {
      let buyingArchives = (<any>data).fields.approxData.archiveIds;

      //Obtaining user ids of user it is needed to NOT exclude
      let buyingUserIds = (<any>this.usersForListUsers)
        .filter(user => user.show)
        .map(user => user.id);

      this.Api.buyOverview(buyingArchives, buyingUserIds).subscribe((res) => {

        //Opening the dialog
        const dialogRef = this.dialog.open(UserBuyOverviewComponent, {
          //width: '100%',
          data: [(<any>res).fields.data, (<any>res).fields.positionCounter]
        });

        //Binding to the close event of the dialog
        dialogRef.afterClosed().subscribe(result => {
          if (result.localeCompare("buy") == 0) {
            this.Api.buyFinal(buyingArchives, buyingUserIds).subscribe((resFinal) => {
              this.Vcs.openSnackbar("Posizioni comprate!");
            });
          }
        });

      });
    });
  }


  //This function gets new Archives and draws circles on map
  updateArchivesStructures() {
    this.Api.getPositions(this.boundObject).subscribe((data) => {
      //console.log(data);
      if((<any>data).success){
        //Resetting structures in order to hold just new data
        this.approxTimeStamps = {};
        this.layers = [];

        let r_users = (<any>data).fields.approxData.users; //Users associative vector. Key: userid, Value: some data.

        for (var r_user in r_users) {
          let color; //Random color generated

          if (this.userColors[r_user] != null) { //If the user was considered before
            color = this.userColors[r_user]; //Then use the already generated color 
          } else { //Else generate a new one
            color = "rgb(" + Math.floor(Math.random() * 255) + ", " + Math.floor(Math.random() * 255) + ", " + Math.floor(Math.random() * 255) + ")";
            this.userColors[r_user] = color; //Save the generated color
          }
          
          //For user selection on map (checkboxes)
          let isThisUserToShow = this.isUserToShow(r_user);

          if (isThisUserToShow == -1) { //If the user wasn't considered before
            this.usersForListUsers.push({ id: r_user, show: true }); //Create a new object and display the user
            isThisUserToShow = 1;
          }

          if (isThisUserToShow == 1) { //If the user was just added or already considered
            let r_appPoints = r_users[r_user].appPoints; //Get his/her approximate positions
            for (var r_appPoint in r_appPoints) { //and draw a circle for every position
              var point = new LatLng(r_appPoints[r_appPoint].y, r_appPoints[r_appPoint].x);
              this.addCircle(point, color);
            }

            //Also save his timestamps to show them in the bubblechart
            this.approxTimeStamps[r_user] = r_users[r_user].appTimestamps;
          }
        } //End for(var r_user in r_users)

        this.generateBubbleChartData(this.approxTimeStamps);
        this.layers.push(this.drawnPolygon); //Since the map was completely cleared we need to add again the polygon the user drawn (if any)
        this.cd.detectChanges();
      }else{ //!(<any>data).success)
        this.Vcs.openSnackbar((<any>data).message);
      }
    });
}


  //This function is binded to the view. It actually doesn't call again the API but just make the points
  //and timestamps disappear.
  checkBoxChange($event, index) {
    this.usersForListUsers[index].show = !this.usersForListUsers[index].show;
    this.updateArchivesStructures();
  }

  //Retriving information about the user
  isUserToShow(userid) {
    let trovato = -1;
    this.usersForListUsers.forEach(user => {
      if (user.id == userid) {
        trovato = (user.show == true) ? 1 : 0;
      }
    });
    return trovato;
  }

  changeDateTime(){
    if(this.endTime > this.startTime){
      this.updateBoundObject(this.GMap);
    }else{
      this.Vcs.openSnackbar("Errore nella selezione delle date!");
    }
  }

  //This function converts the Date object to our formato to send it to the APIs
  convertDate(t: Date){
    //"2020.04.27.9.35.30";
    return (t.getFullYear() + "." + (t.getMonth()+1) + "." + t.getDate() + "." + t.getHours() + "." + t.getMinutes() + "." + t.getSeconds());
  }

  resetPolygon() {
    (<any>this.requestObject).polygon = [];
    this.layers.pop();
    this.drawnPolygon = polygon([]);
    this.layers.push(this.drawnPolygon);
  }


  addCircle(p: any, color: any) {
    const newCircle = circle(
      [p.lat, p.lng],
      {
        radius: 100,
        fillColor: color,
        color: color
      }
    );
    this.layers.push(newCircle);
  }

  addPointInPolygon(p: any, newPosForSpring) {
    this.drawnPolygon.addLatLng([p.lat, p.long]); //Add point
    (<any>this.requestObject).polygon.push(newPosForSpring);
  }


  private getFormattedNow() {

    var a = new Date(Date.now());
    var year = a.getFullYear();
    var month = a.getMonth() + 1;
    var date = a.getDate();
    var hour = a.getHours();
    var min = a.getMinutes();
    var sec = a.getSeconds();
    var time = year + '.' + month + '.' + date + '.' + hour + '.' + min + '.' + sec;
    return time;
  }
}

