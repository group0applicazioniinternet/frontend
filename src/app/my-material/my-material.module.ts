import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//Import Angular Material
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {MatTabsModule} from '@angular/material/tabs'; // For tab in Login/Sign up

import { RouterModule } from '@angular/router'; //For navigation

//Modules from angular material for fancy stuff
import {MatButtonModule,
  MatCheckboxModule,
  MatToolbarModule,
  MatCardModule,
  MatGridListModule,
  MatInputModule,
  MatProgressBarModule,
  MatSnackBarModule,
  MatTableModule,
  MatProgressSpinnerModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatExpansionModule,
  MatDialogModule
} from '@angular/material';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';




@NgModule({
  imports: [
    CommonModule,

    //Angular material
    MatButtonModule, MatCheckboxModule, MatToolbarModule,
    MatCardModule, MatGridListModule, MatFormFieldModule,
    MatIconModule, MatInputModule, MatProgressBarModule,
    MatSnackBarModule, MatTableModule, MatProgressSpinnerModule,
    MatDatepickerModule, MatNativeDateModule, MatExpansionModule,
    MatTabsModule, RouterModule,
    MatDialogModule,
    BrowserAnimationsModule, //Animations
  ],
  exports:[
        //Angular material
        MatButtonModule, MatCheckboxModule, MatToolbarModule,
        MatCardModule, MatGridListModule, MatFormFieldModule,
        MatIconModule, MatInputModule, MatProgressBarModule,
        MatSnackBarModule, MatTableModule, MatProgressSpinnerModule,
        MatDatepickerModule, MatNativeDateModule, MatExpansionModule,
        MatTabsModule, RouterModule,
        MatDialogModule,
        BrowserAnimationsModule, //Animations
  ],
  declarations: []
})
export class MyMaterialModule { }
