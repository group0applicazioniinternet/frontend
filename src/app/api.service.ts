import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  private token: string = null;

  private clientId = "Client1";
  private clientSecret = "Secret1";

  //Save token locally. This function is called from Auth API when loggin in
  useToken(token) {
    this.token = token;
  }

  printToken() {
    console.log(this.token);
  }

  //Getting user archives for user personal page
  getUserArchives() {
    const apiUrl = "http://localhost:8080/user/archives";

    let headers = new HttpHeaders();
    headers = headers.set("Authorization", "Basic " + btoa(this.clientId + ":" + this.clientSecret));
    headers = headers.set("Content-Type", "application/x-www-form-urlencoded");

    let params = new HttpParams();
    params = params.set("access_token", this.token);

    return this.http.get(apiUrl, { headers, params });
  }

  //Adding user archive (from personal page)
  addUserArchives(data) {
    const apiUrl = "http://localhost:8080/user/archives";
    let body = new URLSearchParams();
    body.set("access_token", this.token);
    body.set("positions", data);

    let headers = new HttpHeaders();
    headers = headers.set("Content-Type", "application/x-www-form-urlencoded");

    return this.http.post(apiUrl, body.toString(), { headers: headers });
  }

  //Delete user archive from personal page
  deleteUserArchive(archiveId) {
    const apiUrl = "http://localhost:8080/user/archives/delete";
    let body = new URLSearchParams();
    body.set("access_token", this.token);
    let data = '{"archiveId": "' + archiveId + '"}';
    body.set("myData", data);

    let headers = new HttpHeaders();
    headers = headers.set("Content-Type", "application/x-www-form-urlencoded");

    return this.http.post(apiUrl, body.toString(), { headers: headers });
  }


  //Get user bought archives to show in personal page
  getUserBoughtArchives() {
    const apiUrl = "http://localhost:8080/user/archives/bought";

    let headers = new HttpHeaders();
    headers = headers.set("Authorization", "Basic " + btoa(this.clientId + ":" + this.clientSecret));
    headers = headers.set("Content-Type", "application/x-www-form-urlencoded");

    let params = new HttpParams();
    params = params.set("access_token", this.token);

    return this.http.get(apiUrl, { headers, params });
  }


  //This is related to the map, it returns approximate data
  getPositions(data) {
    const apiUrl = "http://localhost:8080/user/approx/view";
    let body = new URLSearchParams();
    body.set("access_token", this.token);
    body.set("myData", JSON.stringify(data));

    //console.log("Data sent to API: ", JSON.stringify(data));

    let headers = new HttpHeaders();
    headers = headers.set("Content-Type", "application/x-www-form-urlencoded");

    return this.http.post(apiUrl, body.toString(), { headers: headers });
  }


  //This function is called after the click on buy near the map
  //it is called just after a polygon is drawn on the map
  //Receives archive ids the user want to buys and userIds he/she wants to exclude.
  //Returns: A list of archives with a flag that indicates if the user already bought those archives
  //and the total number of positions (not approximate) inside the archives (he need to buy, not the ones he/she
  //already owns)
  buyOverview(archiveIds, userIds) {
    const apiUrl = "http://localhost:8080/user/buy/overview";
    let body = new URLSearchParams();
    body.set("access_token", this.token);
    let data = new Object();
    data["archiveIds"] = archiveIds;
    data["userIds"] = userIds;
    body.set("myData", JSON.stringify(data));

    //console.log("Data sent to API: ", JSON.stringify(data));

    let headers = new HttpHeaders();
    headers = headers.set("Content-Type", "application/x-www-form-urlencoded");

    return this.http.post(apiUrl, body.toString(), { headers: headers });
  }


  //This function is called when the user want to buy the archives shown in the overview dialog.
  //Receives the archives the user wants to buy (even those already bought) and the userIds to exclude.
  //Returns a list of bought archives
  buyFinal(archiveIds, userIds) {
    const apiUrl = "http://localhost:8080/user/buy/final";
    let body = new URLSearchParams();
    body.set("access_token", this.token);
    let data = new Object();
    data["archiveIds"] = archiveIds;
    data["userIds"] = userIds;
    body.set("myData", JSON.stringify(data));

    //console.log("Data sent to API: ", JSON.stringify(data));

    let headers = new HttpHeaders();
    headers = headers.set("Content-Type", "application/x-www-form-urlencoded");

    return this.http.post(apiUrl, body.toString(), { headers: headers });
  }

}